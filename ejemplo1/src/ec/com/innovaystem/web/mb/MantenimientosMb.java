package ec.com.innovaystem.web.mb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import ec.com.innovasystem.dao.ProductoDao;
import ec.com.innovasystem.dao.TipoProductoDao;
import ec.com.innovasystem.entidades.Producto;
import ec.com.innovasystem.entidades.TipoProducto;

@ManagedBean
@SessionScoped
public class MantenimientosMb {

	private TipoProducto tipoProducto;
	private TipoProducto tipoProductoConsulta;
	private Integer idBusqueda;
	public List<TipoProducto> lstTipoProducto;
	
	private Producto productoNuevo;
	private Producto productoConsulta;
	private Integer idProductoConsulta;
	private List<Producto> lstProducto;
	
	
	public MantenimientosMb(){
	}
	
	@ManagedProperty(value = "#{loginMb}")
	private LoginMb loginMb;
	
	@PostConstruct
	public void inicializar(){
		if(loginMb!=null)
			System.out.println(loginMb.getUsuario());
		else
			System.out.println("Sin sesion");
		tipoProducto = new TipoProducto();
		productoNuevo = new Producto();
		this.consultarTodos();
	}
	
	@EJB
	TipoProductoDao tipoProductoDao;
	@EJB
	ProductoDao productoDao;
	
	//Inicio Tipo Producto
	
	public void crear(){
		
		if(this.tipoProducto.getNombre()==null||this.tipoProducto.getNombre().isEmpty()){
			mostrarMensaje("ERROR", 
					"Datos vacios", FacesMessage.SEVERITY_ERROR);
			return;
		}
			
		
		try {
			tipoProductoDao.crear(tipoProducto);
			mostrarMensaje("EXITO", 
					"Se ha guardado exitosamente el tipo de producto "+tipoProducto.getNombre(), FacesMessage.SEVERITY_INFO);
			tipoProducto = new TipoProducto();
			consultarTodos();
		} catch (Exception e) {
			mostrarMensaje("ERROR", 
					"Error al guardar Tipo de producto", FacesMessage.SEVERITY_ERROR);
		}
	}
	
	public void consultarTodos(){
		try {
			lstTipoProducto = tipoProductoDao.consultarTodosTipoProducto();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void consultarTipoProductoXPk(){
		if(idBusqueda!=null){
			try {
				this.tipoProductoConsulta = tipoProductoDao.buscarPorPK(idBusqueda);
			} catch (Exception e) {
				mostrarMensaje("ERROR", 
						"Error al consultar Tipo de producto", FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	public void actualizarTipoProducto(){
		if(this.tipoProductoConsulta!=null){
			try {
				tipoProductoDao.actualizar(tipoProductoConsulta);
				mostrarMensaje("Informacion", 
						"Se ha actualizado Tipo de producto "+tipoProductoConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
				tipoProductoConsulta = new TipoProducto();
				consultarTodos();
			} catch (Exception e) {
				mostrarMensaje("ERROR", 
						"Error al actualizar Tipo de producto", FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	public void eliminar(){
		seleccionarElemento();
		if(tipoProductoConsulta!=null){
			try {
				tipoProductoDao.eliminar(tipoProductoConsulta);
				mostrarMensaje("Informacion", 
						"Se ha eliminado Tipo de producto "+tipoProductoConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
				tipoProductoConsulta = new TipoProducto();
				consultarTodos();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public void seleccionarElemento(){
		this.tipoProductoConsulta = (TipoProducto) valorContextoJSF("#{tipoProducto}", TipoProducto.class); 
	}

	//Fin Tipo Producto
	
	//Inicio Producto
	public void crearProducto(){
		try {
			productoDao.crear(productoNuevo);
			mostrarMensaje("EXITO", 
					"Se ha guardado exitosamente el producto "+productoNuevo.getNombre(), FacesMessage.SEVERITY_INFO);
			productoNuevo = new Producto();
			consultarTodosProducto();
		} catch (Exception e) {
			mostrarMensaje("ERROR", 
					"Error al guardar producto", FacesMessage.SEVERITY_ERROR);
		}
	}
	
	public void consultarTodosProducto(){
		try {
			lstProducto = productoDao.consultarTodosProducto();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void consultarProductoXPk(){
		if(idProductoConsulta!=null){
			try {
				this.productoConsulta = productoDao.buscarPorPK(idProductoConsulta);
			} catch (Exception e) {
				mostrarMensaje("ERROR", 
						"Error al consultar producto", FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	public void actualizarProducto(){
		if(this.productoConsulta!=null){
			try {
				productoDao.actualizar(productoConsulta);
				mostrarMensaje("Informacion", 
						"Se ha actualizado producto "+productoConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
				productoConsulta = new Producto();
				consultarTodos();
			} catch (Exception e) {
				mostrarMensaje("ERROR", 
						"Error al actualizar producto", FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	public void eliminarProducto(){
		seleccionarProducto();
		if(productoConsulta!=null){
			try {
				productoDao.eliminar(productoConsulta);
				mostrarMensaje("Informacion", 
						"Se ha eliminado producto "+productoConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
				productoConsulta = new Producto();
				consultarTodos();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public void seleccionarProducto(){
		this.productoConsulta = (Producto) valorContextoJSF("#{producto}", Producto.class); 
	}
	
	//Fin Producto
	
	/**
	 * M�todo exclusivo de tablas, devuelve el valor del contexto de la aplicaci�n, debe enviarse el valor con la respectiva sintaxis jsf
	 * #{nombreVaribableJSF}
	 * @param claseEsperada Tipo de objeto en la cual se espera que retorne el objeto
	 * @param expresion Expresion para realizar la b�squeda en el contexto, debe tener la sintaxis #{nombreVaribableJSF}   
	 * @return Objeto encontrado o nulo si no existe en el contexto o se env�an mal los par�metros
	 */
	public static <T> Object valorContextoJSF(String expresion, Class<T> claseEsperada){
		Object obj  =null;
		if(claseEsperada!=null)
			obj = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(),
					expresion, claseEsperada);
		return obj;
	}

	public TipoProducto getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(TipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	
	public void mostrarMensaje(String cabecera, String mensaje, Severity tipo){
		 FacesMessage message = new FacesMessage(tipo, cabecera, mensaje);
	        FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public List<TipoProducto> getLstTipoProducto() {
		return lstTipoProducto;
	}

	public void setLstTipoProducto(List<TipoProducto> lstTipoProducto) {
		this.lstTipoProducto = lstTipoProducto;
	}

	public Integer getIdBusqueda() {
		return idBusqueda;
	}

	public void setIdBusqueda(Integer idBusqueda) {
		this.idBusqueda = idBusqueda;
	}

	public TipoProducto getTipoProductoConsulta() {
		return tipoProductoConsulta;
	}

	public void setTipoProductoConsulta(TipoProducto tipoProductoConsulta) {
		this.tipoProductoConsulta = tipoProductoConsulta;
	}

	public Producto getProductoNuevo() {
		return productoNuevo;
	}

	public void setProductoNuevo(Producto productoNuevo) {
		this.productoNuevo = productoNuevo;
	}

	public Producto getProductoConsulta() {
		return productoConsulta;
	}

	public void setProductoConsulta(Producto productoConsulta) {
		this.productoConsulta = productoConsulta;
	}

	public Integer getIdProductoConsulta() {
		return idProductoConsulta;
	}

	public void setIdProductoConsulta(Integer idProductoConsulta) {
		this.idProductoConsulta = idProductoConsulta;
	}

	public List<Producto> getLstProducto() {
		return lstProducto;
	}

	public void setLstProducto(List<Producto> lstProducto) {
		this.lstProducto = lstProducto;
	}

	public LoginMb getLoginMb() {
		return loginMb;
	}

	public void setLoginMb(LoginMb loginMb) {
		this.loginMb = loginMb;
	}
	
}