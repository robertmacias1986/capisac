package ec.com.innovaystem.web.mb;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import ec.com.innovaystem.util.Usuario;

@ManagedBean
@SessionScoped
public class LoginMb {

	private String usuario;
	private String clave;
	private Usuario usuarioL;
	
	public void login(){
		if(usuario==null||usuario.isEmpty()){
			mostrarMensaje("Datos vacios","Usuario vacio", FacesMessage.SEVERITY_ERROR);
			return;
		}
		
		if(clave==null||clave.isEmpty()){
			mostrarMensaje("Datos vacios","Usuario vacio", FacesMessage.SEVERITY_ERROR);
			return;
		}
		
		if(usuario.equalsIgnoreCase("peter")
				&&clave.equalsIgnoreCase("123")){
			mostrarMensaje("Iniciar Sesion","Credenciales correctas", FacesMessage.SEVERITY_INFO);
			try {
				usuarioL = new Usuario(usuario,clave);
				ExternalContext contexto =  FacesContext.getCurrentInstance().getExternalContext();
				contexto.redirect("HolaMundoPrime.xhtml");
				((HttpServletRequest)contexto.getRequest())
					.getSession(false).setAttribute("usuario", usuarioL);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//getFacesContext().getExternalContext().redirect("perfil-usuario.xhtml");
		} else {
			mostrarMensaje("Error al realizar login","Credenciales incorrectas", FacesMessage.SEVERITY_ERROR);
		}
		
	}
	
	public void mostrarMensaje(String cabecera, String mensaje, Severity tipo){
		 FacesMessage message = new FacesMessage(tipo, cabecera, mensaje);
	        FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	public void busquedaAjax(){
		if(usuario!=null&&!usuario.isEmpty()&&usuario.equalsIgnoreCase("091234")){
			mostrarMensaje("Codigo de super usuario","Hola super usuario ", FacesMessage.SEVERITY_WARN);
		}
	}
	
	public void cerrarSesion(){
		ExternalContext contexto =  FacesContext.getCurrentInstance().getExternalContext();
		((HttpServletRequest)contexto.getRequest())
		.getSession(false).removeAttribute("usuario");
		((HttpServletRequest)contexto.getRequest()).getSession(false).invalidate();
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getClave() {
		return clave;
	}


	public void setClave(String clave) {
		this.clave = clave;
	}

	public Usuario getUsuarioL() {
		return usuarioL;
	}

	public void setUsuarioL(Usuario usuarioL) {
		this.usuarioL = usuarioL;
	}

}