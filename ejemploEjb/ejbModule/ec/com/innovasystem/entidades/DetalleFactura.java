package ec.com.innovasystem.entidades;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the detalle_factura database table.
 * 
 */
@Entity
@Table(name="detalle_factura")
@NamedQuery(name="DetalleFactura.findAll", query="SELECT d FROM DetalleFactura d")
public class DetalleFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="detalle_factura_id")
	private Integer detalleFacturaId;

	private Integer cantidad;

	@Column(name="valor_descuento")
	private Float valorDescuento;

	@Column(name="valor_neto")
	private Float valorNeto;

	//bi-directional many-to-one association to Factura
	@ManyToOne
	@JoinColumn(name="factura_id", insertable=false,updatable=false)
	private Factura factura;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="producto_id")
	private Producto producto;

	//bi-directional many-to-one association to TipoImpuesto
	@ManyToOne
	@JoinColumn(name="tipo_impuesto_id")
	private TipoImpuesto tipoImpuesto;

	public DetalleFactura() {
	}

	public Integer getDetalleFacturaId() {
		return this.detalleFacturaId;
	}

	public void setDetalleFacturaId(Integer detalleFacturaId) {
		this.detalleFacturaId = detalleFacturaId;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public float getValorDescuento() {
		return this.valorDescuento;
	}

	public void setValorDescuento(Float valorDescuento) {
		this.valorDescuento = valorDescuento;
	}

	public Float getValorNeto() {
		return this.valorNeto;
	}

	public void setValorNeto(float valorNeto) {
		this.valorNeto = valorNeto;
	}

	public Factura getFactura() {
		return this.factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public TipoImpuesto getTipoImpuesto() {
		return this.tipoImpuesto;
	}

	public void setTipoImpuesto(TipoImpuesto tipoImpuesto) {
		this.tipoImpuesto = tipoImpuesto;
	}

}