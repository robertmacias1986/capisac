package ec.com.innovasystem.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.com.innovasystem.entidades.TipoProducto;

/**
 * Session Bean implementation class TipoProductoDao
 */
@Stateless
@LocalBean
public class TipoProductoDao extends DAO<TipoProducto, Integer>{

    public TipoProductoDao() {
    }
    
    public void crearTipoProducto(TipoProducto tipoProducto){
    	try {
			crear(tipoProducto);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public TipoProducto consultarTipoProducto(Integer id){
    	try {
			return buscarPorPK(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    @SuppressWarnings("unchecked")
	public List<TipoProducto> consultarTodosTipoProducto(){
    	List<TipoProducto> lst = null;
    	try {
    		Query q = getNamedQuery("TipoProducto.findAll");
			lst = (List<TipoProducto>) q.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
		}
    	return lst;
    }
    
    @SuppressWarnings("unchecked")
	public List<TipoProducto> buscarXNombre(String nombre){
    	List<TipoProducto> lst = null;
    	try {
    		Query q = getNamedQuery("TipoProducto.buscarXNombre");
    		q.setParameter("nombre", nombre);
			lst = (List<TipoProducto>) q.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
		}
    	return lst;
    }

}
