package ec.com.innovasystem.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.com.innovasystem.entidades.Producto;

/**
 * Session Bean implementation class TipoProductoDao
 */
@Stateless
@LocalBean
public class ProductoDao extends DAO<Producto, Integer>{

    public ProductoDao() {
    }
    
    public void crearProducto(Producto producto){
    	try {
			crear(producto);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public Producto consultarProducto(Integer id){
    	try {
			return buscarPorPK(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    @SuppressWarnings("unchecked")
	public List<Producto> consultarTodosProducto(){
    	List<Producto> lst = null;
    	try {
    		Query q = getNamedQuery("Producto.findAll");
			lst = (List<Producto>) q.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
		}
    	return lst;
    }
    
    @SuppressWarnings("unchecked")
	public List<Producto> buscarXNombre(String nombre){
    	List<Producto> lst = null;
    	try {
    		Query q = getNamedQuery("Producto.buscarXNombre");
    		q.setParameter("nombre", nombre);
			lst = (List<Producto>) q.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
		}
    	return lst;
    }

}
